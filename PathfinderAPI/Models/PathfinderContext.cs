using Microsoft.EntityFrameworkCore;

namespace PathfinderAPI.Models
{
    public class PathfinderContext : DbContext
    {
        public PathfinderContext(DbContextOptions<PathfinderContext> options)
            : base(options)
        {
        }

        public DbSet<Feat> Feats { get; set; }
        public DbSet<Spell> Spells { get; set; }
 
    }
}