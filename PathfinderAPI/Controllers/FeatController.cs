using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PathfinderAPI.Models;

namespace PathfinderAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FeatController : ControllerBase
    {
        private readonly PathfinderContext _context;
        
        public FeatController(PathfinderContext context)
        {
            _context = context;
        }
        
        // Get api/feat
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Feat>>> GetFeats()
        {
            return await _context.Feats.ToListAsync();
        }
        
        // GET: api/feat/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<Feat>> GetFeat(long id)
        {
            var feat = await _context.Feats.FindAsync(id);

            if (feat == null)
            {
                return NotFound();
            }

            return feat;
        }

        // Get api/feat/type/{type}
        [HttpGet("type/{type}")]
        public async Task<ActionResult<IEnumerable<Feat>>> GetFeatsByType(string type)
        {
            var feats = await _context.Feats.Where(f => f.Type.Equals(type)).ToListAsync();
            if (feats == null)
            {
                return NotFound();

            }
            return feats;
        }
        // Filter feat by name

    }
}