using System;
using PathfinderAPI.Models;

namespace PathfinderAPI.Data.DTO
{
    public class SpellDTO
    {
        public string name { get; set; }
        public string school { get; set; }
        public string subschool { get; set; }
        public string descriptor { get; set; }
        public string spell_level { get; set; }
        public string casting_time { get; set; }
        public string components { get; set; }
        public string costly_components { get; set; }
        public string range { get; set; }
        public string area { get; set; }
        public string effect { get; set; }
        public string targets { get; set; }
        public string duration { get; set; }
        public string dismissible { get; set; }
        public string shapeable { get; set; }
        public string saving_throw { get; set; }
        public string spell_resistence { get; set; }
        public string description { get; set; }
        public string description_formated { get; set; }
        public string source { get; set; }
        public string full_text { get; set; }
        public string verbal { get; set; }
        public string somatic { get; set; }
        public string material { get; set; }
        public string focus { get; set; }
        public string divine_focus { get; set; }
        public string sor { get; set; }
        public string wiz { get; set; }
        public string cleric { get; set; }
        public string druid { get; set; }
        public string ranger { get; set; }
        public string bard { get; set; }
        public string paladin { get; set; }
        public string alchemist { get; set; }
        public string summoner { get; set; }
        public string witch { get; set; }
        public string inquisitor { get; set; }
        public string oracle { get; set; }
        public string antipaladin { get; set; }
        public string magus { get; set; }
        public string adept { get; set; }
        public string deity { get; set; }
        public string SLA_Level { get; set; }
        public string domain { get; set; }
        public string short_description { get; set; }
        public string acid { get; set; }
        public string air { get; set; }
        public string chaotic { get; set; }
        public string cold { get; set; }
        public string curse { get; set; }
        public string darkness { get; set; }
        public string death { get; set; }
        public string disease { get; set; }
        public string earth { get; set; }
        public string electricity { get; set; }
        public string emotion { get; set; }
        public string evil { get; set; }
        public string fear { get; set; }
        public string fire { get; set; }
        public string force { get; set; }
        public string good { get; set; }
        public string language_dependent { get; set; }
        public string lawful { get; set; }
        public string light { get; set; }
        public string mind_affecting { get; set; }
        public string pain { get; set; }
        public string poison { get; set; }
        public string shadow { get; set; }
        public string sonic { get; set; }
        public string water { get; set; }
        public string linktext { get; set; }
        public string id { get; set; }
        public string material_costs { get; set; }
        public string bloodline { get; set; }
        public string patron { get; set; }
        public string mythic_text { get; set; }
        public string augmented { get; set; }
        public string mythic { get; set; }
        public string bloodrager { get; set; }
        public string shaman { get; set; }
        public string psychic { get; set; }
        public string medium { get; set; }
        public string mesmerist { get; set; }
        public string occultist { get; set; }
        public string spiritualist { get; set; }
        public string skald { get; set; }
        public string investigator { get; set; }
        public string hunter { get; set; }
        public string haunt_statistics { get; set; }
        public string ruse { get; set; }
        public string draconic { get; set; }
        public string meditative { get; set; }
        public string summoner_unchained { get; set; }

        public Spell ToSpell()
        {
            return new Spell()
            {
                Id = Convert.ToInt32(id),
                Name = name,
                School = school,
                Subschool = subschool,
                Descriptor = descriptor,
                SpellLevel = spell_level,
                CastingTime = casting_time,
                Components = components,
                CostlyComponents = Tools.StringToBool(costly_components),
                Range = range,
                Area = area,
                Effect = effect,
                Targets = targets,
                Duration = duration,
                Dismissible = dismissible,
                Shapeable = shapeable,
                SavingThrow = saving_throw,
                SpellResistence = spell_resistence,
                Description = description,
                DescriptionFormatted = description_formated,
                Source = source,
                FullText = full_text,
                Deity = deity,
                SlaLevel = Convert.ToInt32(SLA_Level),
                Domain = domain,
                ShortDescription = short_description,

                // Spell components
                Verbal = verbal,
                Somatic = somatic,
                Material = material,
                Focus = focus,
                DivineFocus = divine_focus,

                // Todo finish this stuff

                // Core classes
                Sor = Stoi(sor),
                Wiz = Stoi(wiz),
                Cleric = Stoi(cleric),
                Druid = Stoi(druid),
                Ranger = Stoi(ranger),
                Bard = Stoi(bard),
                Paladin = Stoi(paladin),
                
                // Advanced classes
                Alchemist = Stoi(alchemist),
                Summoner = Stoi(summoner),
                Witch = Stoi(witch),
                Inquisitor = Stoi(inquisitor),
                Oracle = Stoi(oracle),
                Antipaladin = Stoi(antipaladin),
                Magus = Stoi(magus),
                Adept = Stoi(adept),
                Bloodrager = Stoi(bloodrager),
                Shaman = Stoi(shaman),
                Psychic = Stoi(psychic),
                Medium = Stoi(medium),
                Mesmerist = Stoi(mesmerist),
                Occultist = Stoi(occultist),
                Spiritualist = Stoi(spiritualist),
                Skald = Stoi(skald),
                Investigator = Stoi(investigator),
                Hunter = Stoi(hunter),
                
                // Types
                Acid = Tools.StringToBool(acid),
                Air = Tools.StringToBool(air),
                Chaotic = Tools.StringToBool(chaotic),
                Cold = Tools.StringToBool(cold),
                Curse = Tools.StringToBool(curse),
                Darkness = Tools.StringToBool(darkness),
                Death = Tools.StringToBool(death),
                Disease = Tools.StringToBool(disease),
                Earth = Tools.StringToBool(earth),
                Electricity = Tools.StringToBool(electricity),
                Emotion = Tools.StringToBool(emotion),
                Evil = Tools.StringToBool(evil),
                Fear = Tools.StringToBool(fear),
                Fire = Tools.StringToBool(fire),
                Force = Tools.StringToBool(force),
                Good = Tools.StringToBool(good),
                LanguageDependent = Tools.StringToBool(language_dependent),
                Lawful = Tools.StringToBool(lawful),
                Light = Tools.StringToBool(light),
                MindAffecting = Tools.StringToBool(mind_affecting),
                Pain = Tools.StringToBool(pain),
                Poison = Tools.StringToBool(poison),
                Shadow = Tools.StringToBool(shadow),
                Sonic = Tools.StringToBool(sonic),
                Water = Tools.StringToBool(water),
                MaterialCosts = Stoi(material_costs),
                Bloodline = Tools.StringToBool(bloodline),
                Patron = Tools.StringToBool(patron),
                MythicText = mythic_text,
                Augmented = Tools.StringToBool(augmented),
                Mythic = Tools.StringToBool(mythic),
            };
        }

        private int? Stoi(string s)
        {
            try
            {
                return Convert.ToInt32(s);
            }
            catch (System.FormatException)
            {
                return null;
            }
            catch (System.OverflowException)
            {
                return null;
            }
        }
    }
}