using System;
using PathfinderAPI.Data.DTO;

namespace PathfinderAPI.Models
{
    public class Spell
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string School { get; set; }
        public string Subschool { get; set; }
        public string Descriptor { get; set; }
        public string SpellLevel { get; set; }
        public string CastingTime { get; set; }
        public string Components { get; set; }
        public bool CostlyComponents { get; set; }
        public string Range { get; set; }
        public string Area { get; set; }
        public string Effect { get; set; }
        public string Targets { get; set; }
        public string Duration { get; set; }
        public string Dismissible { get; set; }
        public string Shapeable { get; set; }
        public string SavingThrow { get; set; }
        public string SpellResistence { get; set; }
        public string Description { get; set; }
        public string DescriptionFormatted { get; set; }
        public string Source { get; set; }
        public string FullText { get; set; }
        public string Verbal { get; set; }
        public string Somatic { get; set; }
        public string Material { get; set; }
        public string Focus { get; set; }
        public string DivineFocus { get; set; }
        public string Deity { get; set; }
        public int SlaLevel { get; set; }
        public string Domain { get; set; }
        public string ShortDescription { get; set; }
        
        // Core classes
        public int? Sor { get; set; }
        public int? Wiz { get; set; }
        public int? Cleric { get; set; }
        public int? Druid { get; set; }
        public int? Ranger { get; set; }
        public int? Bard { get; set; }
        public int? Paladin { get; set; }
        public int? Adept { get; set; }
        
        // Advanced classes
        public int? Alchemist { get; set; }
        public int? Summoner { get; set; }
        public int? Witch { get; set; }
        public int? Inquisitor { get; set; }
        public int? Oracle { get; set; }
        public int? Antipaladin { get; set; }
        public int? Magus { get; set; }
        
        // Element Types
        public bool Acid { get; set; }
        public bool Air { get; set; }
        public bool Chaotic { get; set; }
        public bool Cold { get; set; }
        public bool Curse { get; set; }
        public bool Darkness { get; set; }
        public bool Death { get; set; }
        public bool Disease { get; set; }
        public bool Earth { get; set; }
        public bool Electricity { get; set; }
        public bool Emotion { get; set; }
        public bool Evil { get; set; }
        public bool Fear { get; set; }
        public bool Fire { get; set; }
        public bool Force { get; set; }
        public bool Good { get; set; }
        public bool LanguageDependent { get; set; }
        public bool Lawful { get; set; }
        public bool Light { get; set; }
        public bool MindAffecting { get; set; }
        public bool Pain { get; set; }
        public bool Poison { get; set; }
        public bool Shadow { get; set; }
        public bool Sonic { get; set; }
        public bool Water { get; set; }
        public int? MaterialCosts { get; set; }
        public bool Bloodline { get; set; }
        public bool Patron { get; set; }
        public string MythicText { get; set; }
        public bool Augmented { get; set; }
        public bool Mythic { get; set; }
        
        // Hybrid classes
        public int? Bloodrager { get; set; }
        public int? Shaman { get; set; }
        public int? Psychic { get; set; }
        public int? Medium { get; set; }
        public int? Mesmerist { get; set; }
        public int? Occultist { get; set; }
        public int? Spiritualist { get; set; }
        public int? Skald { get; set; }
        public int? Investigator { get; set; }
        public int? Hunter { get; set; }
        
        public string HauntStatistics { get; set; }
        public string Ruse { get; set; }
        public string Draconic { get; set; }
        public string Meditative { get; set; }
        public string  SummonerUnchained { get; set; }
    }
}