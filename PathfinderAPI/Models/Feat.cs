namespace PathfinderAPI.Models
{
    public class Feat
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Prerequisite { get; set; }
        public string Benefit { get; set; }
        public string Normal { get; set; }
        public string Special { get; set; }
        public string Source { get; set; }
        public bool Critical { get; set; }
        public bool Teamwork { get; set; }
        public bool Grit { get; set; }
        public bool Style { get; set; }
        public bool Performance { get; set; }
        public bool Racial { get; set; }
        public bool CompanionFamiliar { get; set; }
        public bool Multiples { get; set; }
    }
}