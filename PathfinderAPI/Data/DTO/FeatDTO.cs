using System;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using PathfinderAPI.Models;

namespace PathfinderAPI.Data.DTO
{
    public class FeatDTO
    {
        public string id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public string prerequisites { get; set; }
        public string prerequisite_feats { get; set; }
        public string benefit { get; set; }
        public string normal { get; set; }
        public string special { get; set; }
        public string source { get; set; }
        public string fulltext { get; set; }
        public string teamwork { get; set; }
        public string critical { get; set; }
        public string grit { get; set; }
        public string style { get; set; }
        public string performance { get; set; }
        public string racial { get; set; }
        public string companion_familiar { get; set; }
        public string race_name { get; set; }
        public string note { get; set; }
        public string goal { get; set; }
        public string completion_benefit { get; set; }
        public string multiples { get; set; }
        public string suggested_traits { get; set; }
        

        public Feat ToFeat()
        {
            return new Feat()
            {
                Id = Convert.ToInt64(id),
                Name = name,
                Benefit = benefit,
                Prerequisite = prerequisites,
                Normal = normal,
                Type = type,
                Special = special,
                Source = source,
                Critical = Tools.StringToBool(critical),
                CompanionFamiliar = Tools.StringToBool(companion_familiar),
                Grit = Tools.StringToBool(grit),
                Multiples = Tools.StringToBool(multiples),
                Performance = Tools.StringToBool(performance),
                Racial = Tools.StringToBool(race_name),
                Style = Tools.StringToBool(style),
                
            };
        }

    }
}