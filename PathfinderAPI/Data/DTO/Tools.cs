using System;

namespace PathfinderAPI.Data.DTO
{

    public static class Tools
    {
        public static bool StringToBool(string s)
        {
            return s == "1";
        }

        public static int? StringToInt(string s)
        {
            try
            {
                return Convert.ToInt32(s);
            }
            catch (FormatException)
            {
                return null;
            }

            catch (OverflowException)
            {
                return null;
            }
        }
        
    }
}