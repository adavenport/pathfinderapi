using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PathfinderAPI.Models;

namespace PathfinderAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpellController : ControllerBase
    {
        private readonly PathfinderContext _context;
        
        public SpellController(PathfinderContext context)
        {
            _context = context;
        }
        
        // Get api/spell
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Spell>>> GetSpells()
        {
            return await _context.Spells.ToListAsync();
        }
        
        // GET: api/spell/id
        [HttpGet("{id}")]
        public async Task<ActionResult<Spell>> GetTodoItem(long id)
        {
            var spell = await _context.Spells.FindAsync(id);
            if (spell == null)
            {
                return NotFound();
            }
            return spell;
        }

        // Filter spell by type
        // Filter spell by name

    }
}
