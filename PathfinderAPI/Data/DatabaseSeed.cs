using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using CsvHelper;
using PathfinderAPI.Data.DTO;
using PathfinderAPI.Models;

namespace PathfinderAPI.Data
{
    public class DatabaseSeeder
    {
        private const string SPELLS_PATH = "spells.csv";
        private const string FEATS_PATH = "feats.csv";
        private const string MONSTERS_PATH = "monsters.csv";
        private const string NPC_PATH = "npcs.csv";
        private readonly PathfinderContext _context;
        private readonly string _directory;

        public DatabaseSeeder(string directory, PathfinderContext context)
        {
            _directory = directory;
            _context = context;
        }

        public void Start()
        {
            PopulateFeats();
            PopulateSpells();
        }

        private void PopulateFeats()
        {
            var path = Path.Combine(_directory, FEATS_PATH);
            TextReader reader = new StreamReader(path);
            var csvReader = new CsvReader(reader);
            var feats = csvReader.GetRecords<FeatDTO>();
            foreach (var feat in feats)
            {
                try
                {
                    var f = feat.ToFeat();
                    _context.Add(f);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            _context.SaveChanges();
        }

        private void PopulateSpells()
        {
            var path = Path.Combine(_directory, SPELLS_PATH);
            TextReader reader = new StreamReader(path);
            var csvReader = new CsvReader(reader);
            var spells = csvReader.GetRecords<SpellDTO>();
            foreach (var spell in spells)
            {
                try
                {
                    var s = spell.ToSpell();
                    _context.Spells.Add(s);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            _context.SaveChanges();
        }
    }
}